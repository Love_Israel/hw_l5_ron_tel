#include "Menu.h"
#include "Point.h"
#include "Shape.h"
#include "Circle.h"
#include "Triangle.h"
#include "Rectangle.h"
#include "Canvas.h"
#include "Arrow.h"
#include "Polygon.h"
#include <iostream>
#include <vector>

void addNewShape();
void Actions();
void deleteVector();
void eraseBoerd();
void drawAllShapes();

std::vector<Shape*> shapes; //vector contiens shape object ptr
Shape* currShape;
Menu menu = Menu();
Canvas canvas = Canvas();


int main()
{
	int choise;
	bool cont = true;

	while (cont) 
	{
		menu.printMain();
		std::cin >> choise;

		switch (choise) 
		{
		case 0:
			menu.printAdding();
			addNewShape();
			break;
		case 1:
			menu.printShapes(shapes);
			Actions();
			break;
		case 2:
			eraseBoerd();
			shapes.clear();
			break;
		case 3:
			deleteVector();
			cont = false;
			break;
		default:
			std::cout << "wrong choise";
		}
	}
	return 0;
}

void addNewShape () 
{
	int choise;
	int num;
	int num2;
	std::string name;
	Point p = Point (0 , 0);
	std::vector<Point> points;
	std::cin >> choise;


	switch (choise) 
	{
	case 0:
		std::cout << "enter center point x , center point y , radius and name (by that order)";
		p = menu.inputPoint();
		std::cin >> num;
		std::cin >> name;
		currShape = new Circle(p, num, "Circle" , name); //new circle object
		shapes.push_back(currShape); //pushing object ptr to shapes
		break;
	case 1:
		std::cout << "enter x1,y1,x2,y2 , name (in that order) ";
		p = menu.inputPoint();
		points.push_back(p);
		p = menu.inputPoint();
		points.push_back(p);
		std::cin >> name;
		currShape = new Arrow(points[0], points[1], "Arrow", name);
		shapes.push_back(currShape);
		break;
	case 2:
		std::cout << "enter x1,y1,x2,y2,x3,y3 , name (in that order) ";
		for (int i : {0, 1, 2}) { //inputing 3 points
			p = menu.inputPoint();
			points.push_back(p);
		}
		if ((points[0].getX() == points[1].getX() && points[1].getX() == points[2].getX()) || points[0].getY() == points[1].getY() && points[1].getY() == points[2].getY()) //if one line
			goto end;
		std::cin >> name;
		currShape = new Triangle(points[0], points[1], points[2], "Triangle" , name);
		shapes.push_back(currShape);
		break;
	case 3:
		std::cout << "enter x1,y1 width , length , name (int that order) ";
		p = menu.inputPoint();
		std::cin >> num;
		std::cin >> num2;
		if (num2 == 0 || num == 0) //if width / lenth == 0 terminate
			goto end;
		std::cin >> name;
		currShape = new myShapes::Rectangle(p, num, num2, "Rectangle", name);
		shapes.push_back(currShape);
		break;
	default:
		std::cout << "wrong choise";
	}

	shapes[shapes.size() - 1]->draw(canvas);
	drawAllShapes();
	end: {}
}

void Actions () 
{
	if (shapes.empty())
		return;
	int shape;
	int choise;
	Point p = Point(0, 0);
	std::cin >> shape;
	menu.printActions();
	std::cin >> choise;
	switch (choise) 
	{
	case 0:
		std::cout << "enter x scala , y scala ";
		p = menu.inputPoint();
		shapes[shape]->move(p); //move shape
		eraseBoerd(); //erase all boerd
		drawAllShapes(); //re-draw all shapes
		break;
	case 1:
		shapes[shape]->printDetails();
		std::cout << shapes[shape]->getArea() << "\n";
		std::cout << shapes[shape]->getPerimeter() << "\n";
		break;
	case 2:
		shapes[shape]->clearDraw(canvas); //erase shape fron boerd
		shapes.erase(shapes.begin() + shape); //erase shape from vector
		break;
	default:
		std::cout << "wrong choise ";
	}
}

void eraseBoerd() 
{
	for (int i = 0 ; i < shapes.size() ; i++) 
	{
		shapes[i]->clearDraw(canvas); //for some reasone func dosent work , dont erase enything
	}
}

void deleteVector() 
{
	for (int i = 0; i < shapes.size(); i++)
	{
		delete (shapes[i]); //free to ptr
	}
}

void drawAllShapes () 
{
	for (int i = 0; i < shapes.size(); i++)
	{
		shapes[i]->draw(canvas); //draw the shape
	}
}

