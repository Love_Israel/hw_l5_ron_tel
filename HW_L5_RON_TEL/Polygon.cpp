#include "Polygon.h"
#include <iostream>

//move is the same for triangle and rectangle , so it defined here
void Polygon :: move (const Point &other) 
{
	
	for (int i = 0 ; i < _points.size() ; i++)
	{
		_points[i] += other;
	}
}

Polygon :: Polygon(const std::string& type, const std::string& name) : Shape (name , type)
{
	_name = name;
	_type = type;
}



