#include "Menu.h"
#include <iostream>
#include <vector>

Menu::Menu() 
{
}

Menu::~Menu()
{
}

void Menu ::printMain () 
{
	std::cout << "Welcome ! to this useless shape generator !\n enter 0 to add new shape\n enter 1 to modify or get info from shape\n enter 2 to delete all shapes \n enter 3 to exit. ";
}

void Menu ::printAdding  () 
{
	std::cout << "Enter 0 to add a circle\nEnter 1 to add an arrow\nEnter 2 to add a triangle\nEnter 3 to add a rectangle.";
}

void Menu ::printShapesInfo (std::vector<Shape*> shapes) 
{
	for (auto i : shapes) 
	{
		std::cout << i->getName() << " " << i->getType() << "\n area is " << i->getArea() << "\n perimeter is " << i->getPerimeter() << "\n";
	}
}

void Menu::printShapes(std::vector<Shape*> shapes) 
{
	int counter = 0;
	for (auto i : shapes) //iterate all shapes 
	{
		std::cout << "Enter " << counter << " for " << i->getName() << "  (" << i->getType() << ") \n";
		counter++;
	}
}

void Menu ::printActions () 
{
	std::cout << "Enter 0 to move the shape\nEnter 1 to get its details\nEnter 2 to remove the shape\n";
}

Point& Menu :: inputPoint () 
{
	int x;
	int y;
	Point* p;
	std::cin >> x;
	std::cin >> y;
	p = new Point(x, y);
	return *p;
}