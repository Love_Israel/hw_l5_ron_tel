#include "Rectangle.h"
#include <iostream>



void myShapes::Rectangle::draw(const Canvas& canvas)
{
	canvas.draw_rectangle(_points[0], _points[1]);
}

void myShapes::Rectangle::clearDraw(const Canvas& canvas)
{
	canvas.clear_rectangle(_points[0], _points[1]);
}

myShapes :: Rectangle :: Rectangle(const Point& a, double length, double width, const std::string& type, const std::string& name) : Polygon(type , name)
{
	_points.push_back(a);
	Point p = Point(a.getX(), a.getY() - width);
	_points.push_back(p);
	p = Point(a.getX() - length, a.getY());
	_points.push_back(p);
	p = Point(a.getX() - length, a.getY() - width);
	_points.push_back(p);
	_name = name;
	_type = type;
}

myShapes ::Rectangle ::~Rectangle () 
{

}

double myShapes :: Rectangle :: getArea() const
{
	
	return ((_points[0].getY() - _points[1].getY()) * (_points[1].getX() - _points[2].getX()));
}

double myShapes :: Rectangle :: getPerimeter() const 
{
	return ((_points[0].getY() - _points[1].getY()) *2 + ((_points[1].getX() - _points[2].getX())*2));
}
