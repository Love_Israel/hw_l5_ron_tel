#include "Point.h"
#include <math.h>
#include <iostream>

Point :: Point(double x, double y) 
{
	X = x;
	Y = y;
}

Point :: Point (const Point& other) 
{
	X = other.getX();
	Y = other.getY();
}

Point ::~Point () 
{
	//nothing to delete
}

//overload + operator
Point Point::operator+ (const Point& other) const 
{
	double x = X;
	double y = Y;
	x += other.getX();
	y += other.getY();
	Point* p = new Point(x, y);
	return *p;
}

//overload += operator
Point& Point :: operator+= (Point const& other) 
{
	X += other.X;
	Y += other.Y;
	return *this;
}

double Point :: distance (const Point& other) const 
{
	return (sqrt(int(other.getX() - X) ^ 2 + int(other.getY() - Y) ^ 2)); //according to pitagoras
}

double Point ::getX () const 
{
	return X;
}

double Point::getY() const
{
	return Y;
}



