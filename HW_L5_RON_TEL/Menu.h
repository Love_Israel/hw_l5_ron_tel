#pragma once
#include "Shape.h"
#include "Canvas.h"
#include "Point.h"
#include <vector>



class Menu
{
public:

	Menu();
	~Menu();
	void printMain();
	void printAdding();
	void printShapesInfo(std::vector<Shape*> shapes);
	void printShapes(std::vector <Shape*> shapes);
	void printActions();
	Point& inputPoint();

private: 
	Canvas _canvas;
};

