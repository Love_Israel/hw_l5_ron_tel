#include "Arrow.h"

void Arrow::draw(const Canvas& canvas)
{
	canvas.draw_arrow(_points[0], _points[1]);

}
void Arrow::clearDraw(const Canvas& canvas)
{
	canvas.draw_arrow(_points[0], _points[1]);
}

//ctor for Arrow , intialize Shape
Arrow :: Arrow (const Point& a, const Point& b, const std::string& type, const std::string& name) : Shape (name , type)
{
	_points.push_back(a);
	_points.push_back(b);
	_name = name;
	_type = type;
}


Arrow :: ~Arrow () 
{
	//nothing to delete...
}

double Arrow :: getArea() const 
{
	return 0;
}

double Arrow :: getPerimeter () const 
{
	return _points[0].distance(_points[1]);
}

//adding points values , i would use automatic vars but it dosent work
void Arrow :: move(const Point& other) 
{
	for (int i = 0 ; i < 2 ; i++) 
	{
		_points[i] += other;
	}
}




